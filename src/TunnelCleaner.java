import kareltherobot.Robot;
import java.util.Arrays;
import kareltherobot.World;
import javax.swing.JOptionPane;
import java.util.Scanner;
public class TunnelCleaner extends Robot {
	private int[] tunnelStartLoc;
	public static void main()
	{
		
	}
	public TunnelCleaner(int i, int j, Direction dir) {
		super(i, j, dir,0);
	}
/**
 * This function takes the TunnelCleaner to the entrance of the
 * tunnel and leaves it facing into the tunnel one step outside
 * We do not know the starting location nor the starting direction 
 * of the Robot.
 */
	public void findTunnel(Robot r) 
	{
		if (r.facingEast()==true)
		{
			r.turnLeft();
		}
		else if (r.facingWest()==true)
		{
			r.turnLeft();
			r.turnLeft();
			r.turnLeft();
		}
		else if (r.facingSouth()==true)
		{
			r.turnLeft();
			r.turnLeft();
		}
		while (true)
		{
			while (r.frontIsClear()==true)
			{
				r.move();
			}
			r.turnLeft();
			while (r.frontIsClear()==true)
			{
				r.turnLeft();
				if (r.frontIsClear()==false)
				{
					r.turnLeft();
					r.turnLeft();
					if (r.frontIsClear()==false)
					{
						r.turnLeft();
						r.turnLeft();
						r.turnLeft();
						r.move();
						//r.turnOff();
						r.turnLeft();
						r.turnLeft();
						r.turnLeft();
						r.turnLeft();
						r.turnLeft();
						return;
					}
					else
					{
						r.turnLeft();
						if (r.frontIsClear()==true)
						{
							r.move();
						}
						else
						{
							r.turnLeft();
						}
					}
				}
				else 
				{
					r.turnLeft();
					if (r.frontIsClear())
					{
						r.move();
					}
					else
					{
						r.turnLeft();
					}
				}
			}
		}
	}
		

	/**
	 * This function takes the TunnelCleaner to the end of the
	 * tunnel and leaves it facing the exit.
	 * We know the bot starts at the entrance of the tunnel, facing
	 * in.  @return returns the number of steps it took to get
	 * to the end
	 * @return 
	 */
	public int navigateTunnel(Robot r) {
		//System.out.println(startingCount);
		//r.turnLeft();
		boolean moved = false;
		int count = 0;
		while (true)
		{
			r.turnLeft();
			moved = false;
			
			while (r.frontIsClear()==true)
			{
				moved = true;
				r.move();
				if (r.nextToABeeper()==true)
				{
					count++;
					System.out.println(count);
				}
			}
			r.turnLeft();
			if (r.frontIsClear()==true && moved==false)
			{
				moved = true;
				while (r.frontIsClear()==true)
				{
					r.move();
					if (r.nextToABeeper()==true)
					{
						count++;
						System.out.println(count);
					}
				}
			}
			r.turnLeft();
			r.turnLeft();
			if (r.frontIsClear()==true && moved==false)
			{
				moved = true;
				while (r.frontIsClear()==true)
				{
					r.move();
					if (r.nextToABeeper()==true)
					{
						count++;
						System.out.println(count);
					}
				}
			}
			if (moved == false)
			{
				r.turnLeft();
				r.turnLeft();
				r.turnLeft();
				return count;
			}
			
		}
	}
	/**
	 * This method takes the robot from the end of the tunnel to the 
	 * entrance.  It has this Robot pick up piles of beepers along the
	 * way.  For each pile of beepers, it places the pile size into the
	 * specified array of ints at a location that corresponds to the 
	 * depth that the robot currently is in the tunnel.  Finally, the 
	 * location of the biggest pile is returned by this function, thus
	 * requiring the robot to save its location 
	 * @param beeperPiles The array to be filled.  Assume the length of
	 * the array equals the number of steps this Robot is from the entrance
	 * @return This method returns an array of int that has length of 2 
	 * and contains the street and avenue, respectively.
	 */
	public void clearAndClean(Robot r, int num) 
	{
		int count=1;
		if (r.facingEast()==true)
		{
			r.turnLeft();
		}
		else if (r.facingWest()==true)
		{
			r.turnLeft();
			r.turnLeft();
			r.turnLeft();
		}
		else if (r.facingSouth()==true)
		{
			r.turnLeft();
			r.turnLeft();
		}
		while (true)
		{
		while (true)
		{
			if (r.frontIsClear()==false)
			{
				if (r.facingEast()==true)
				{
					r.turnLeft();
					r.turnLeft();
				}
				else if (r.facingSouth()==true)
				{
					r.turnLeft();
				}
				else if (r.facingNorth()==true)
				{
					r.turnLeft();
					r.turnLeft();
					r.turnLeft();
				}
			}
			r.turnLeft();
			r.turnLeft();
			if (r.frontIsClear()==true)
			{
				while (r.frontIsClear()==true)
				{
					r.move();
				}
				break;
			}
			r.turnLeft();
			if (r.frontIsClear()==true)
			{
				while (r.frontIsClear()==true)
				{
					r.move();
				}
				break;
			}
			r.turnLeft();
			r.turnLeft();
			if (r.frontIsClear()==true)
			{
				while (r.frontIsClear()==true)
				{
					r.move();
				}
				break;
			}
		}
		}
	}
	
}
