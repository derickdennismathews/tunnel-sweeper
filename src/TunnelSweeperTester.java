import java.util.Scanner;

import kareltherobot.Directions;
import kareltherobot.Robot;
import kareltherobot.World;

public class TunnelSweeperTester implements Directions{

	public static void main(String[] args) {
		new TunnelSweeperTester().start();
	}
	
	private int steps;// depth of tunnel
	private int[] beeperPiles;// array containing all pile heights
	private int[] bigPileLoc;// array with two ints [street, ave]
	private int[] tunnelEntrance; // array with two ints [street, ave]

	private void start() {
		System.out.println("enter world name");
		Scanner myObj = new Scanner(System.in);  // Create a Scanner object
		String name = myObj.nextLine();
		
		name = name + ".wld";
		String[] robotInfo = promptForRobotInfo();
		World.readWorld(name);
		World.setVisible(true);
		// create TunnelCleaner with info from line 12
		TunnelCleaner tc = makeBot(robotInfo);
		
		sweepTunnel(tc);// might need to pass in our robot
		displayResults();
	}
	private String[] promptForRobotInfo() {
		Scanner myObj = new Scanner(System.in);  // Create a Scanner object
	    System.out.println("Enter stret(x) Coordinate");
	    String street = myObj.nextLine();	
	    System.out.println("Enter avenue(y) Coordinate");
	    String avenue = myObj.nextLine();	
	    System.out.println("Enter Direction");
	    String dir = myObj.nextLine();
	    String[] info = {street,avenue, dir};
	    return info;
	}
	private String promptForWorld() {
		Scanner myObj = new Scanner(System.in);  // Create a Scanner object
		String name = myObj.nextLine();
		return name;
	}
	/**
	 * This method parses the Strings in robotInfo and return
	 * a robot with that info
	 * @param robotInfo array of Strings with 3 elements
	 *        robotInfo[0] will represent street, 
	 *        robotInfo[1] avenue
	 *        robotInfo[2] direction
	 * @return TunnelCleaner initialized with specified info
	 */
	private TunnelCleaner makeBot(String[] robotInfo) {
		int street =Integer.parseInt(robotInfo[0]);
		int avenue = Integer.parseInt(robotInfo[1]);
		Direction dir = null;
		if (robotInfo[2].equals("North"))
		{
			dir = North;
		}
		else if (robotInfo[2].equals("South"))
		{
			dir = South;
		}
		else if (robotInfo[2].equals("East"))
		{
			dir = East;
		}
		else if (robotInfo[2].equals("West"))
		{
			dir = West;
		}
		TunnelCleaner robot = new TunnelCleaner(street, avenue, dir);
		return robot;
	}

	/**
	 * Display the following info:
	 * 
	 * Location of the tunnel entrance
	 * Depth of the tunnel
	 * Highest pile size and location of the highest pile
	 * Total # of beepers, beeper piles and average pile size
	 */
	private void displayResults() {
		
		
	}

	private void sweepTunnel(TunnelCleaner tc) {
		World.setDelay(5);
		tc.findTunnel(tc);
		int num = tc.navigateTunnel(tc);
		tc.clearAndClean(tc,num);
	}

}
